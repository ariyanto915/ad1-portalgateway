package com.infoflow.portalgateway.web.rest.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ResponseModel<T> {

	@JsonProperty("responseCode")
	private String responseCode;

	@JsonProperty("message")
	private String responseDesc;

	@JsonProperty("data")
	private T dataResponse;

	public ResponseModel() {

	}

	public ResponseModel(String responseCode, String responseDesc, T dataResponse) {
		super();
		this.responseCode = responseCode;
		this.responseDesc = responseDesc;
		this.dataResponse = dataResponse;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public T getDataResponse() {
		return dataResponse;
	}

	public void setDataResponse(T dataResponse) {
		this.dataResponse = dataResponse;
	}

}

