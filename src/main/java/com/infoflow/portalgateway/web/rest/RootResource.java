package com.infoflow.portalgateway.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

/**
 * REST controller for managing Gateway configuration.
 */
@RestController
@RequestMapping("/")
public class RootResource {

    @Value("${application.base_url}")
    private String baseUrl;
    /**
     * GET  /: Root
     *
     * @return Redirect to index.html
     */
    @GetMapping("${application.base_url}")
    @Timed
    public RedirectView getBase() {
        return new RedirectView(baseUrl + "/index.html");
    }
}
