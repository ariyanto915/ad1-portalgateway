package com.infoflow.portalgateway.gateway.accesscontrol;

import com.infoflow.common.kafka.KafkaTopic;
import com.infoflow.common.utils.CommonUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import io.github.jhipster.config.JHipsterProperties;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Zuul filter for restricting access to backend micro-services endpoints.
 */
public class AccessControlFilter extends ZuulFilter {
    // TODO: delete this after moving to AOP / AspectJ
    @Value("${spring.kafka.topic.update-user-last-activity}")
    private String topic;

    @Autowired
    private Producer<String, Map> producer;

    private final Logger log = LoggerFactory.getLogger(AccessControlFilter.class);

    private final RouteLocator routeLocator;

    private final JHipsterProperties jHipsterProperties;

    public AccessControlFilter(RouteLocator routeLocator, JHipsterProperties jHipsterProperties) {
        this.routeLocator = routeLocator;
        this.jHipsterProperties = jHipsterProperties;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
    	String requestUri = RequestContext.getCurrentContext().getRequest().getRequestURI();
    	String contextPath = RequestContext.getCurrentContext().getRequest().getContextPath();
    	//TODO : Hari : ini direvert dulu
        /*HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
        String requestUri = request.getRequestURI();
        String contextPath = request.getContextPath();*/
        // If the request Uri does not start with the path of the authorized endpoints, we block the request
        for (Route route : routeLocator.getRoutes()) {
            String serviceUrl = contextPath + route.getFullPath();
            String serviceName = route.getId();

            // If this route correspond to the current request URI
            // We do a substring to remove the "**" at the end of the route URL
            if (requestUri.startsWith(serviceUrl.substring(0, serviceUrl.length() - 2))) {
            	return !isAuthorizedRequest(serviceUrl, serviceName, requestUri);

                /*boolean authorizedRequest = isAuthorizedRequest(serviceUrl, serviceName, requestUri);

				//TODO : Hari : ini direvert dulu
                // TODO: Change this to using AOP / AspectJ
                try {
                    String authorization = request.getHeader("Authorization");
                    String authSplit[] = authorization.split(" ");
                    String type = authSplit[0];
                    String value = authSplit[1];
                    if (type.equalsIgnoreCase("Bearer")) {
                        Map<String, Object> tokenMap = CommonUtils.decodeJWTToken(value);

                        String username = tokenMap.get("user_name").toString();

                        KafkaTopic kafkaTopic = new KafkaTopic(topic);
                        Map<String, String> userPayload = new HashMap<>();
                        userPayload.put("username", username);

                        Callback producerCallback = (message, exception) -> {
                            if(exception != null) {
                                log.error("[" + AccessControlFilter.class + "]", exception);
                            } else {
                                log.info("[" + AccessControlFilter.class + "] " + userPayload);
                            }
                        };

                        producer.send(kafkaTopic.createRecord(userPayload), producerCallback);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return authorizedRequest;*/
            }
        }
        return true;
    }

    private boolean isAuthorizedRequest(String serviceUrl, String serviceName, String requestUri) {
        Map<String, List<String>> authorizedMicroservicesEndpoints = jHipsterProperties.getGateway()
            .getAuthorizedMicroservicesEndpoints();

        // If the authorized endpoints list was left empty for this route, all access are allowed
        if (authorizedMicroservicesEndpoints.get(serviceName) == null) {
            log.debug("Access Control: allowing access for {}, as no access control policy has been set up for " +
                "service: {}", requestUri, serviceName);
            return true;
        } else {
            List<String> authorizedEndpoints = authorizedMicroservicesEndpoints.get(serviceName);

            // Go over the authorized endpoints to control that the request URI matches it
            for (String endpoint : authorizedEndpoints) {
                // We do a substring to remove the "**/" at the end of the route URL
                String gatewayEndpoint = serviceUrl.substring(0, serviceUrl.length() - 3) + endpoint;
                if (requestUri.startsWith(gatewayEndpoint)) {
                    log.debug("Access Control: allowing access for {}, as it matches the following authorized " +
                        "microservice endpoint: {}", requestUri, gatewayEndpoint);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.setResponseStatusCode(HttpStatus.FORBIDDEN.value());
        ctx.setSendZuulResponse(false);
        log.debug("Access Control: filtered unauthorized access on endpoint {}", ctx.getRequest().getRequestURI());
        return null;
    }
}
