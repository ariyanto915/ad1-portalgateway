package com.infoflow.portalgateway.config.properties;

public class ServiceAuthenticationProperties {

    private String baseUrl;

    private String apiKey;

    private PathApiProperties pathApi;

    private UserSystemProperties userSystem;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public PathApiProperties getPathApi() {
        return pathApi;
    }

    public void setPathApi(PathApiProperties pathApi) {
        this.pathApi = pathApi;
    }

    public UserSystemProperties getUserSystem() {
        return userSystem;
    }

    public void setUserSystem(UserSystemProperties userSystem) {
        this.userSystem = userSystem;
    }
}
