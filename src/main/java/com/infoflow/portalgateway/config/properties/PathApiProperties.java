package com.infoflow.portalgateway.config.properties;

public class PathApiProperties {

    private String menu;

    private String verifyOtp;

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getVerifyOtp() {
        return verifyOtp;
    }

    public void setVerifyOtp(String verifyOtp) {
        this.verifyOtp = verifyOtp;
    }
}
