package com.infoflow.portalgateway.config;

import com.infoflow.portalgateway.config.properties.ServiceAuthenticationProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Portal Gateway.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
	private boolean development_mode = false;
    private String baseUrl;

    private ServiceAuthenticationProperties serviceAuthentication;

	public boolean isDevelopment_mode() {
		return development_mode;
	}

	public void setDevelopment_mode(boolean development_mode) {
		this.development_mode = development_mode;
	}

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public ServiceAuthenticationProperties getServiceAuthentication() {
        return serviceAuthentication;
    }

    public void setServiceAuthentication(ServiceAuthenticationProperties serviceAuthentication) {
        this.serviceAuthentication = serviceAuthentication;
    }
}
