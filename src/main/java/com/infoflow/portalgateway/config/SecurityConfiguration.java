package com.infoflow.portalgateway.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.loadbalancer.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.CorsFilter;

import com.infoflow.portalgateway.config.oauth2.OAuth2JwtAccessTokenConverter;
import com.infoflow.portalgateway.config.oauth2.OAuth2Properties;
import com.infoflow.portalgateway.security.AuthoritiesConstants;
import com.infoflow.portalgateway.security.oauth2.OAuth2SignatureVerifierClient;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends ResourceServerConfigurerAdapter {
	private final Logger log = LoggerFactory.getLogger(SecurityConfiguration.class);

    private final OAuth2Properties oAuth2Properties;

    private final CorsFilter corsFilter;

    private final ApplicationProperties appProp;

    public SecurityConfiguration(OAuth2Properties oAuth2Properties, CorsFilter corsFilter
    		, ApplicationProperties appProp) {
        this.oAuth2Properties = oAuth2Properties;
        this.corsFilter = corsFilter;
        this.appProp = appProp;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
    	boolean isDevelopmentMode = appProp.isDevelopment_mode();
    	log.debug("MicroserviceSecurityConfiguration dev mode " + isDevelopmentMode);

    	if(isDevelopmentMode) {
    		http
		        .csrf().disable()
	            .authorizeRequests()
	            .antMatchers(org.springframework.http.HttpMethod.OPTIONS, "/auth/login").permitAll()
	            .antMatchers(org.springframework.http.HttpMethod.OPTIONS, "/auth/logout").permitAll()
	            .antMatchers(org.springframework.http.HttpMethod.OPTIONS, "/idm/api/**").permitAll()
	            .antMatchers(org.springframework.http.HttpMethod.OPTIONS, "/api/profile-info").permitAll()
	            .antMatchers(org.springframework.http.HttpMethod.OPTIONS, "/api/**").permitAll()
	            .antMatchers(org.springframework.http.HttpMethod.OPTIONS, "**/api/**").permitAll()
	            .antMatchers("/management/health").permitAll()
	            .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN);
    	} else {
    		http
	            .csrf()
	            .ignoringAntMatchers("/h2-console/**")
	            .ignoringAntMatchers("/auth/login")
	            .ignoringAntMatchers("/auth/logout")
	            .ignoringAntMatchers("/idm/api/MNU_IDM_CHG_PASSWD/getMinPassword")
	            .ignoringAntMatchers("/idm/api/MNU_IDM_CHG_PASSWD/forceChangePassword")
                .ignoringAntMatchers("/foundation/api/MNU_MT_SYS_PARAM/customer_otp_timeout")
                .ignoringAntMatchers("/foundation/api/MNU_MT_SYS_PARAM/getSessionTimeout")
                .ignoringAntMatchers("/idm/api/MNU_IDM_USER/validateToken")
                .ignoringAntMatchers("/voucher/api/customer/otp/submit")
                .ignoringAntMatchers("/loan-product/api/customer/otp/submit")
                .ignoringAntMatchers("/loan-product/api/customer/otp/verify")
                .ignoringAntMatchers("/loan-product/privy/callback-document-status")
	            .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
	        .and()
	            .addFilterBefore(corsFilter, CsrfFilter.class)
	            .headers()
	            .frameOptions()
	            .disable()
	        .and()
	            .sessionManagement()
	            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	        .and()
	            .authorizeRequests()
	            .antMatchers("/api/**").authenticated()
	            .antMatchers("**/api/**").authenticated()
	            .antMatchers("/management/health").permitAll()
	            .antMatchers("/management/info").permitAll()
	            .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN);
    	}
    }

    @Bean
    public TokenStore tokenStore(JwtAccessTokenConverter jwtAccessTokenConverter) {
        return new JwtTokenStore(jwtAccessTokenConverter);
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter(OAuth2SignatureVerifierClient signatureVerifierClient) {
        return new OAuth2JwtAccessTokenConverter(oAuth2Properties, signatureVerifierClient);
    }

    @Bean
	@Qualifier("loadBalancedRestTemplate")
    public RestTemplate loadBalancedRestTemplate(RestTemplateCustomizer customizer) {
        RestTemplate restTemplate = new RestTemplate();
        customizer.customize(restTemplate);
        return restTemplate;
    }

    @Bean
    @Qualifier("vanillaRestTemplate")
    public RestTemplate vanillaRestTemplate() {
        return new RestTemplate();
    }
}
