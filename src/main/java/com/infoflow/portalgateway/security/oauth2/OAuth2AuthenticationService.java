package com.infoflow.portalgateway.security.oauth2;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.infoflow.common.ProductOTPTypeEnum;
import com.infoflow.common.api.ApiContext;
import com.infoflow.common.api.ParamHolder;
import com.infoflow.common.api.RestAPIUtil;
import com.infoflow.common.constants.CommonConstants;
import com.infoflow.common.constants.ConstantAPI;
import com.infoflow.common.exception.BusinessException;
import com.infoflow.common.utils.CommonUtils;
import com.infoflow.common.utils.DateTimeFunction;
import com.infoflow.portalgateway.config.properties.ServiceAuthenticationProperties;
import com.infoflow.portalgateway.web.rest.util.ResponseModel;

import io.github.jhipster.security.PersistentTokenCache;
import liquibase.util.StringUtils;

/**
 * Manages authentication cases for OAuth2 updating the cookies holding access and refresh tokens accordingly.
 * <p>
 * It can authenticate users, refresh the token cookies should they expire and log users out.
 */
public class OAuth2AuthenticationService {

    private final Logger log = LoggerFactory.getLogger(OAuth2AuthenticationService.class);

    private final RestTemplate loadBalancedRestTemplate;
    private final ServiceAuthenticationProperties serviceAuthenticationProperties;

    /**
     * Number of milliseconds to cache refresh token grants so we don't have to repeat them in case of parallel requests.
     */
    private static final long REFRESH_TOKEN_VALIDITY_MILLIS = 10000l;

    /**
     * Used to contact the OAuth2 token endpoint.
     */
    private final OAuth2TokenEndpointClient authorizationClient;

    /**
     * Helps us with cookie handling.
     */
    private final OAuth2CookieHelper cookieHelper;

    /**
     * Caches Refresh grant results for a refresh token value so we can reuse them.
     * This avoids hammering UAA in case of several multi-threaded requests arriving in parallel.
     */
    private final PersistentTokenCache<OAuth2Cookies> recentlyRefreshed;

    public OAuth2AuthenticationService(RestTemplate loadBalancedRestTemplate, ServiceAuthenticationProperties serviceAuthenticationProperties,
                                       OAuth2TokenEndpointClient authorizationClient, OAuth2CookieHelper cookieHelper) {
        this.loadBalancedRestTemplate = loadBalancedRestTemplate;
        this.serviceAuthenticationProperties = serviceAuthenticationProperties;
        this.authorizationClient = authorizationClient;
        this.cookieHelper = cookieHelper;
        recentlyRefreshed = new PersistentTokenCache<>(REFRESH_TOKEN_VALIDITY_MILLIS);
    }

    private Map<String, Object> getPayloadVerifyOTP(Map<String, String> params) {
        return new HashMap<String, Object>() {
            {
                put("type", ProductOTPTypeEnum.CUSTOMER.name());
                put("code", com.infoflow.common.utils.StringUtils.formatPhoneNumber(params.get("noHp")));
                put("OTP", params.get("otp"));
                put("timestamp", DateTimeFunction.getCurrentTimeInEpochMilliseconds());
            }
        };
    }

    public ResponseEntity<Object> authenticateByOTP(HttpServletRequest request, HttpServletResponse response, Map<String, String> params) throws BusinessException {
        String pathApiVerifyOTP = ConstantAPI.ROOT + ConstantAPI.IdmAPI.Otp.OTP + ConstantAPI.IdmAPI.Otp.VERIFY_OTP;
        String url = serviceAuthenticationProperties.getBaseUrl().concat(pathApiVerifyOTP);

        Map<String, Object> payload = this.getPayloadVerifyOTP(params);
        ApiContext<Map, Object> apiContext = new ApiContext<>(url, payload, Object.class);
        String apiKey = serviceAuthenticationProperties.getApiKey();
        ParamHolder paramHolder = new ParamHolder("api-key", apiKey);

        if(log.isDebugEnabled()) {
        	log.debug("Start authenticateByOTP request to {} with has api-key : {}", pathApiVerifyOTP, (org.apache.commons.lang.StringUtils.isEmpty(apiKey)?"NO":"YES"));	
        }
        // call api verify otp
        ResponseEntity<Object> responseEntity = RestAPIUtil.getInstance(loadBalancedRestTemplate)
            .postForEntity(apiContext, Collections.singletonList(paramHolder), MediaType.APPLICATION_JSON);
        if(log.isDebugEnabled()) {
        	log.debug("End authenticateByOTP request to {} with response : {}", responseEntity.toString());	
        }
        // when otp verified, then generate token access using user system
        if(responseEntity.getStatusCode().equals(HttpStatus.OK)) {
        	if(log.isDebugEnabled()) {
            	log.debug("authenticateByOTP --> generate token access");	
            }
            Map result = CommonUtils.convertObjectToMap(responseEntity.getBody());
            if(result.get("responseCode").equals(CommonConstants.MSG_SUCCESS)) {
                Map<String, String> payloadLogin = new HashMap<String, String>() {
                    {
                        put("username", serviceAuthenticationProperties.getUserSystem().getUsername());
                        put("password", serviceAuthenticationProperties.getUserSystem().getPassword());
                        put("phone_number", com.infoflow.common.utils.StringUtils.formatPhoneNumber(String.valueOf(payload.get("code"))));
                    }
                };
                return this.authenticate(request, response, payloadLogin);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ResponseModel<String>(CommonConstants.MSG_FAILED, result.get("message").toString(), null));
            }
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ResponseModel<String>(CommonConstants.MSG_FAILED, "Failed Authenticate", null));
        }
    }

    /**
     * Authenticate the user by username and password.
     *
     * @param request  the request coming from the client.
     * @param response the response going back to the server.
     * @param params   the params holding the username, password and rememberMe.
     * @return the OAuth2AccessToken as a ResponseEntity. Will return OK (200), if successful.
     * If the UAA cannot authenticate the user, the status code returned by UAA will be returned.
     */
    public ResponseEntity<Object> authenticate(HttpServletRequest request, HttpServletResponse response, Map<String, String> params) {
    	String username = params.get("username");
        String password = params.get("password");
        String appCode = params.get("app_code");
        String phoneNumber = params.get("phone_number");
    	try {
            boolean rememberMe = true; //set to true to make sure refresh token

            log.debug("contacting OAuth2 token endpoint to login user: {}", username);
            TokenHolder tokenHolder;
            if(StringUtils.isEmpty(phoneNumber)) {
                tokenHolder = authorizationClient.sendPasswordGrant(username, password);
            } else {
                tokenHolder = authorizationClient.sendPasswordGrant(username, password, phoneNumber);
            }

            this.userValidation(appCode, tokenHolder);

            OAuth2Cookies cookies = new OAuth2Cookies();
            cookieHelper.createCookies(request, tokenHolder.getAccessToken(), rememberMe, cookies);
            cookies.addCookiesTo(response);
            if (log.isDebugEnabled()) {
                log.debug("successfully authenticated user {}", params.get("username"));
            }
            return ResponseEntity.ok(tokenHolder.getAccessToken());
        } catch (HttpClientErrorException ex) {
            log.error("failed to get OAuth2 tokens from IDM", ex);
            try {
            	this.failedLogin(username, password);
            } catch (Exception e) {
            	log.error("Invalid User");
            }
            ObjectMapper mapper = new ObjectMapper();
            Map result;

            String generalErrorMessage = "Invalid User ID or Password";
			try {
				result = mapper.readValue(ex.getResponseBodyAsString(), Map.class);
			} catch (JsonParseException e1) {
				log.error("JSON Parse ERROR");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	            		.body(new ResponseModel<>(null, generalErrorMessage, null));
			} catch (JsonMappingException e1) {
				log.error("JSON Mapping ERROR");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	            		.body(new ResponseModel<>(null, generalErrorMessage, null));
			} catch (IOException e1) {
				log.error("IO Exception");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	            		.body(new ResponseModel<>(null, generalErrorMessage, null));
			}

			if(Objects.nonNull(result) && Objects.nonNull(result.get("message"))) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
	            		.body(new ResponseModel<>(null,
	            				(String) result.get("message"), null));

			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	            		.body(new ResponseModel<>(null, generalErrorMessage, null));
			}
        }
    }

    private void userValidation(String appCode, TokenHolder tokenHolder) {
        tokenHolder.getFormParams().set("access_token", tokenHolder.getAccessToken().getValue());
        tokenHolder.getFormParams().set("refresh_token", tokenHolder.getAccessToken().getRefreshToken().getValue());
        tokenHolder.getFormParams().set("app_code", appCode);

        /*
         * additional user validation
         */
        HttpHeaders userReqHeaders = new HttpHeaders();
        userReqHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<MultiValueMap<String, String>> userEntity = new HttpEntity<>(tokenHolder.getFormParams(), userReqHeaders);
        loadBalancedRestTemplate.postForEntity(this.getUrlValidation(), userEntity, com.infoflow.common.utils.ResponseModel.class);

        /*
         * call success login API
         */
        loadBalancedRestTemplate.postForEntity(this.getUrlSuccessLogin(), userEntity, com.infoflow.common.utils.ResponseModel.class);
    }

    /**
     * Try to refresh the access token using the refresh token provided as cookie.
     * Note that browsers typically send multiple requests in parallel which means the access token
     * will be expired on multiple threads. We don't want to send multiple requests to UAA though,
     * so we need to cache results for a certain duration and synchronize threads to avoid sending
     * multiple requests in parallel.
     *
     * @param request       the request potentially holding the refresh token.
     * @param response      the response setting the new cookies (if refresh was successful).
     * @param refreshCookie the refresh token cookie. Must not be null.
     * @return the new servlet request containing the updated cookies for relaying downstream.
     */
    public HttpServletRequest refreshToken(HttpServletRequest request, HttpServletResponse response, Cookie
        refreshCookie) {
        //check if non-remember-me session has expired
        if (cookieHelper.isSessionExpired(refreshCookie)) {
            log.info("session has expired due to inactivity");
            logout(request, response);       //logout to clear cookies in browser
            return stripTokens(request);            //don't include cookies downstream
        }
        OAuth2Cookies cookies = getCachedCookies(refreshCookie.getValue());
        synchronized (cookies) {
            //check if we have a result from another thread already
            if (cookies.getAccessTokenCookie() == null) {            //no, we are first!
                //send a refresh_token grant to UAA, getting new tokens
                String refreshCookieValue = OAuth2CookieHelper.getRefreshTokenValue(refreshCookie);
                OAuth2AccessToken accessToken = authorizationClient.sendRefreshGrant(refreshCookieValue);
                boolean rememberMe = OAuth2CookieHelper.isRememberMe(refreshCookie);
                cookieHelper.createCookies(request, accessToken, rememberMe, cookies);
                //add cookies to response to update browser
                cookies.addCookiesTo(response);
            } else {
                log.debug("reusing cached refresh_token grant");
            }
            //replace cookies in original request with new ones
            CookieCollection requestCookies = new CookieCollection(request.getCookies());
            requestCookies.add(cookies.getAccessTokenCookie());
            requestCookies.add(cookies.getRefreshTokenCookie());
            return new CookiesHttpServletRequestWrapper(request, requestCookies.toArray());
        }
    }

    /**
     * Get the result from the cache in a thread-safe manner.
     *
     * @param refreshTokenValue the refresh token for which we want the results.
     * @return a RefreshGrantResult for that token. This will either be empty, if we are the first one to do the
     * request,
     * or contain some results already, if another thread already handled the grant for us.
     */
    private OAuth2Cookies getCachedCookies(String refreshTokenValue) {
        synchronized (recentlyRefreshed) {
            OAuth2Cookies ctx = recentlyRefreshed.get(refreshTokenValue);
            if (ctx == null) {
                ctx = new OAuth2Cookies();
                recentlyRefreshed.put(refreshTokenValue, ctx);
            }
            return ctx;
        }
    }

    /**
     * Logs the user out by clearing all cookies.
     *
     * @param httpServletRequest  the request containing the Cookies.
     * @param httpServletResponse the response used to clear them.
     */
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
    	Object loginId = httpServletRequest.getSession().getAttribute("loginId");
    	logout(loginId);

        cookieHelper.clearCookies(httpServletRequest, httpServletResponse);
    }

    /**
     * Strips token cookies preventing them from being used further down the chain.
     * For example, the OAuth2 client won't checked them and they won't be relayed to other services.
     *
     * @param httpServletRequest the incoming request.
     * @return the request to replace it with which has the tokens stripped.
     */
    public HttpServletRequest stripTokens(HttpServletRequest httpServletRequest) {
        Cookie[] cookies = cookieHelper.stripCookies(httpServletRequest.getCookies());
        return new CookiesHttpServletRequestWrapper(httpServletRequest, cookies);
    }

	protected void logout(Object loginId) {
    	if(loginId != null) {
    		log.debug("Logout : " + loginId.toString());
    		this.logout(loginId.toString());
    	}
	}

    public void failedLogin(String username, String password) {
        /*
         * call failed login API
         */
        MultiValueMap<String, String> formParams = new LinkedMultiValueMap<>();
        formParams.set("username", username);
        formParams.set("password", password);
        formParams.set("grant_type", "password");
        HttpHeaders userReqHeaders = new HttpHeaders();
        userReqHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<MultiValueMap<String, String>> userEntity = new HttpEntity<>(formParams, userReqHeaders);
        loadBalancedRestTemplate.postForEntity(this.getUrlUserFailedlogin(), userEntity, com.infoflow.common.utils.ResponseModel.class);
    }

    public void logout(String username) {
        /*
         * call failed logout API
         */
        MultiValueMap<String, String> formParams = new LinkedMultiValueMap<>();
        formParams.set("username", username);
        formParams.set("grant_type", "password");
        HttpHeaders userReqHeaders = new HttpHeaders();
        userReqHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<MultiValueMap<String, String>> userEntity = new HttpEntity<>(formParams, userReqHeaders);
        loadBalancedRestTemplate.postForEntity(this.getUrlUserLogout(), userEntity, com.infoflow.common.utils.ResponseModel.class);
    }

    private String getUrlValidation() {
        return this.getBasePathApiUser() + CommonConstants.USER_VALIDATION_API;
    }

    private String getUrlSuccessLogin() {
        return this.getBasePathApiUser() + CommonConstants.USER_SUCCESS_LOGIN_API;
    }

    private String getUrlUserFailedlogin() {
        return this.getBasePathApiUser() + CommonConstants.USER_FAILED_LOGIN_API;
    }

    private String getUrlUserLogout() {
        return this.getBasePathApiUser() + CommonConstants.USER_LOGOUT_API;
    }

    private String getBasePathApiUser() {
        return serviceAuthenticationProperties.getBaseUrl()
            + ConstantAPI.ROOT
            + ConstantAPI.IdmAPI.User.USER;
    }

}
