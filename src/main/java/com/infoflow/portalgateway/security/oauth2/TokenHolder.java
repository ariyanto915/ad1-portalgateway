package com.infoflow.portalgateway.security.oauth2;

import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.util.MultiValueMap;

public class TokenHolder {

    private OAuth2AccessToken accessToken;

    private MultiValueMap<String, String> formParams;

    public TokenHolder() {
    }

    public TokenHolder(OAuth2AccessToken accessToken, MultiValueMap<String, String> formParams) {
        this.accessToken = accessToken;
        this.formParams = formParams;
    }

    public OAuth2AccessToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(OAuth2AccessToken accessToken) {
        this.accessToken = accessToken;
    }

    public MultiValueMap<String, String> getFormParams() {
        return formParams;
    }

    public void setFormParams(MultiValueMap<String, String> formParams) {
        this.formParams = formParams;
    }
}
