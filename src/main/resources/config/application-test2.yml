# ===================================================================
# Spring Boot configuration for the "dev" profile.
#
# This configuration overrides the application.yml file.
#
# More information on profiles: https://www.jhipster.tech/profiles/
# More information on configuration properties: https://www.jhipster.tech/common-application-properties/
# ===================================================================

# ===================================================================
# Standard Spring Boot properties.
# Full reference is available at:
# http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html
# ===================================================================

logging:
    level:
        ROOT: ERROR
        io.github.jhipster: INFO
        com.infoflow.portalgateway: INFO

eureka:
    instance:
        prefer-ip-address: true
    client:
        service-url:
            defaultZone: http://admin:${jhipster.registry.password}@10.50.3.102:8761/eureka/

spring:
    profiles:
        active: test
        include: swagger
    devtools:
        restart:
            enabled: true
        livereload:
            enabled: false # we use Webpack dev server + BrowserSync for livereload
    jackson:
        serialization.indent_output: true
    datasource:
        type: com.zaxxer.hikari.HikariDataSource
        url: jdbc:postgresql://10.50.3.104:5432/portal_gateway2-13
        username: portal_gateway
        password: infoflow
        hikari:
          idle-timeout: 10000
          maximumPoolSize: 10
          minimumIdle: 1
          maximumIdle: 3
    jpa:
        database-platform: io.github.jhipster.domain.util.FixedPostgreSQL82Dialect
        database: POSTGRESQL
        show-sql: false
        properties:
            hibernate.id.new_generator_mappings: true
            hibernate.cache.use_second_level_cache: true
            hibernate.cache.use_query_cache: false
            hibernate.generate_statistics: true
            hibernate.cache.region.factory_class: com.hazelcast.hibernate.HazelcastCacheRegionFactory
            hibernate.cache.hazelcast.instance_name: portal-gateway
            hibernate.cache.use_minimal_puts: true
            hibernate.cache.hazelcast.use_lite_member: true
    data:
        elasticsearch:
            properties:
                path:
                    home: target/star/portal-gateway/log
    liquibase:
        contexts: dev
    mail:
        host: localhost
        port: 25
        username:
        password:
    messages:
        cache-duration: PT1S # 1 second, see the ISO 8601 standard
    thymeleaf:
        cache: false
    zipkin: # Use the "zipkin" Maven profile to have the Spring Cloud Zipkin dependencies
        base-url: http://localhost:9411
        enabled: false
        locator:
            discovery:
                enabled: true
    kafka:
        topic:
            update-user-last-activity: UPDATE-USER-LAST-ACTIVITY
        broker: 10.50.3.102
        consumer:
            group-id: GROUP-ID-HERE
        bootstrap-servers: 10.50.3.102:9092
# ===================================================================
# To enable SSL in development, uncomment the the "server.ssl" properties below.
#
# JHipster has generated a self-signed certificate, which will be used to encrypt traffic.
# As your browser will not understand this certificate, you will need to import it.
#
# Another (easiest) solution with Chrome is to enable the "allow-insecure-localhost" flag
# at chrome://flags/#allow-insecure-localhost
# ===================================================================
server:
    port: 4500
#    ssl:
#        key-store: classpath:config/tls/keystore.p12
#        key-store-password: password
#        key-store-type: PKCS12
#        key-alias: selfsigned

# ===================================================================
# JHipster specific properties
#
# Full reference is available at: https://www.jhipster.tech/common-application-properties/
# ===================================================================

jhipster:
    gateway:
        rate-limiting:
            enabled: false
            limit: 100000
            duration-in-seconds: 3600
        authorized-microservices-endpoints: # Access Control Policy, if left empty for a route, all endpoints will be accessible
            app1: /api,/v2/api-docs # recommended dev configuration
            loan-product: /api, /privy/callback-document-status
            foundation: /api
            merchant: /api
    http:
        version: V_1_1 # To use HTTP/2 you will need SSL support (see above the "server.ssl" configuration)
    cache: # Cache configuration
        hazelcast: # Hazelcast distributed cache
            time-to-live-seconds: 3600
            backup-count: 1
            management-center: # Full reference is available at: http://docs.hazelcast.org/docs/management-center/3.9/manual/html/Deploying_and_Starting.html
                enabled: false
                update-interval: 3
                url: http://localhost:8180/mancenter
    # CORS is only enabled by default with the "dev" profile, so BrowserSync can access the API
    #cors:
        #allowed-origins: "*"
        #allowed-methods: "*"
        #allowed-headers: "*"
        #exposed-headers: "Authorization,Link,X-Total-Count"
        #allow-credentials: true
        #max-age: 1800
    security:
        client-authorization:
            access-token-uri: http://idm/oauth/token
            token-service-id: idm
            client-id: star-client-service
            client-secret: 5T@r-53Rv1C3$
    mail: # specific JHipster mail property, for standard properties see MailProperties
        from: portal-gateway@localhost
        base-url: http://127.0.0.1:4500
    metrics: # DropWizard Metrics configuration, used by MetricsConfiguration
        jmx:
            enabled: true
        logs: # Reports Dropwizard metrics in the logs
            enabled: false
            report-frequency: 60 # in seconds
    logging:
        logstash: # Forward logs to logstash over a socket, used by LoggingConfiguration
            enabled: false
            host: localhost
            port: 5000
            queue-size: 512

oauth2:
    signature-verification:
        public-key-endpoint-uri: http://idm/oauth/token_key
        #ttl for public keys to verify JWT tokens (in ms)
        ttl: 3600000
        #max. rate at which public keys will be fetched (in ms)
        public-key-refresh-rate-limit: 10000
    web-client-configuration:
        #keep in sync with UAA configuration
        client-id: curl-client
        secret: gendut123
        # Controls session expiration due to inactivity (ignored for remember-me).
        # Negative values disable session inactivity expiration.
        session-timeout-in-seconds: 1800

# ===================================================================
# Application specific properties
# Add your own application properties here, see the ApplicationProperties class
# to have type-safe configuration, like in the JHipsterProperties above
#
# More documentation is available at:
# https://www.jhipster.tech/common-application-properties/
# ===================================================================

application:
    development_mode: false
    base_url: /backoffice
    service-authentication:
        base-url: http://idm
        api-key: $2a$10$xrW5BAi.wJ99jVM4IslHxOuksqMlMKDzw5gTYZt4e0Mwc1Kg52JT6
        path-api:
            menu: /MNU_IDM_USER
            verify-otp: /checkOTP
        user-system:
            username: system1
            password: P@ssw0rd

idm:
    base_url: http://idm/api/MNU_IDM_USER
