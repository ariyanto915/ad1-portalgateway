## Portal Gateway
Portal Gateway provides HTTP routing, security and load balancing.

When the gateways and the microservices are launched, it will register themselves to the **Registry (MS)** (using the eureka.client.serviceUrl.defaultZone key in the src/main/resources/config/application.yml file).

If there are several instances of the same service running, the gateway will get those instances from the JHipster Registry, and will:

1. Load balance HTTP requests using Netflix Ribbon.
2. Provide a circuit breaker using Netflix Hystrix, so that failed instances are quickly and safely removed.


### Development mode

To **compile** your application:

1. go to your project path
2. type mvn clean install -Dmaven.test.skip=true



To **start** your application :

1. go to your project path
2. go to "target" folder
3. type java -Xms100m -Xmx300m -jar <warFile> --spring.profiles.active=dev,no-liquibase
