@echo off

set defaultWar=portal-gateway-0.0.1-SNAPSHOT.war
set warFile=%1

echo Start Server

cd target

IF [%1]==[] (
	echo run %defaultWar%
	java -Xms100m -Xmx300m -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=3332 -jar %defaultWar% --spring.profiles.active=dev
) ELSE (
	echo run %warFile%
	java -Xms100m -Xmx300m -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=3332 -jar %warFile% --spring.profiles.active=dev
)


cd..
